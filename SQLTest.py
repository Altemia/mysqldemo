#------------------------------------------------------------------
## MySQL Python Test
#------------------------------------------------------------------
import mysql.connector
from mysql.connector import Error

global mydb
global mycursor

#------------------------------------------------------------------
def CreateDatabase():
	print("Start CreateDatabase()")
	global mycursor
	mycursor.execute("CREATE DATABASE mydatabase")

#------------------------------------------------------------------
def CheckDatabaseExists(in_databaseName):
	print("Start CheckDatabaseExists()")
	global mycursor
	mycursor.execute("SHOW DATABASES")

	for x in mycursor:
		if in_databaseName in x:
			return True
	return False

#------------------------------------------------------------------
def CheckTableExists(in_tableName):
	print("Start CheckTableExists()")
	global mycursor
	mycursor.execute("SHOW TABLES")

	for x in mycursor:
		if in_tableName in x:
			return True
	return False

#------------------------------------------------------------------
def ShowDatabases():
	global mycursor
	print("Start ShowDatabases()")
	mycursor.execute("SHOW DATABASES")

	for x in mycursor:
	  print(x)

#------------------------------------------------------------------
def ShowTables():
	global mycursor
	print("Start ShowTables()")
	mycursor.execute("SHOW TABLES")

	for x in mycursor:
	  print(x)

#------------------------------------------------------------------
def ShowHills(in_tableName):
	print("Start ShowHills(in_tableName)")
	global mycursor
	mycursor.execute("SELECT * FROM " + in_tableName)
	myresult = mycursor.fetchall()
	for x in myresult:
	  print(x)

#------------------------------------------------------------------
def CreateHillsTable(in_databaseName):
	print("Start CreateHillsTable()")
	global mycursor
	mycursor.execute("USE " + in_databaseName);
	DeleteTable("hills")
	if CheckTableExists("hills") is False:
		print("CreateHillsTable, Hills did not exist")
		mycursor.execute("CREATE TABLE hills (name VARCHAR(255),mountain_range VARCHAR(255),height_mtrs smallint, PRIMARY KEY(name))") 

#------------------------------------------------------------------
def DeleteTable(in_tableName):
	print("Start DeleteTable(in_tableName)")
	global mycursor
	if CheckTableExists("in_tableName") is True:
		mycursor.execute("DROP TABLE " + in_tableName)

#------------------------------------------------------------------
def Init():
	print("Start Init()")
	global mydb
	global mycursor
	mydb = mysql.connector.connect(host="127.0.0.1",user="root",passwd="H0rnsw0ggle!!", port="3306")
	mycursor = mydb.cursor(buffered=True)

#------------------------------------------------------------------
def AddToTable():
	global mycursor
	sql = "INSERT INTO hills (name, mountain_range, height_mtrs) VALUES (%s, %s, %s)"
	val = ("Ben Lomond", "Grampians", "974")
	mycursor.execute(sql, val)

#------------------------------------------------------------------
## Main Method.
#------------------------------------------------------------------
def main():
	Init()
	ShowDatabases()
	if CheckDatabaseExists("mydatabase") is False:
		CreateDatabase()
	CreateHillsTable("mydatabase")
	AddToTable()
	ShowTables()
	ShowHills("hills")
	print("Finished!")

#------------------------------------------------------------------
## Main Method.
#------------------------------------------------------------------
if __name__== "__main__":
	main()